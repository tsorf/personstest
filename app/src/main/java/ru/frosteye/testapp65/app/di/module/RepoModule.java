package ru.frosteye.testapp65.app.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.testapp65.data.repo.contract.PersonsRepo;
import ru.frosteye.testapp65.data.repo.impl.PersonsRepoSPImpl;
import ru.frosteye.testapp65.data.repo.impl.PersonsRepoSqliteImpl;

@Module
public class RepoModule {

    /*@Provides @Singleton
    PersonsRepo providePersonsRepo(PersonsRepoSPImpl repo) {
        return repo;
    }*/

    @Provides @Singleton
    PersonsRepo providePersonsRepo(PersonsRepoSqliteImpl repo) {
        return repo;
    }
}
