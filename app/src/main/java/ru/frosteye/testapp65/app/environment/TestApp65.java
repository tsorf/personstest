package ru.frosteye.testapp65.app.environment;

import android.app.Application;

import ru.frosteye.testapp65.app.di.component.AppComponent;
import ru.frosteye.testapp65.app.di.component.DaggerAppComponent;
import ru.frosteye.testapp65.app.di.module.AppModule;


public class TestApp65 extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
