package ru.frosteye.testapp65.app.di.module;

import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.app.environment.TestApp65;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BaseAppModule;
import ru.frosteye.testapp65.execution.exchange.common.Api;
import ru.frosteye.testapp65.execution.exchange.common.ApiClient;
import ru.frosteye.testapp65.execution.exchange.common.RetrofitClient;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.*;


@Module
public class AppModule extends BaseAppModule<TestApp65> {

    public AppModule(TestApp65 context) {
        super(context);
    }

    @Provides @Singleton
    Api provideApi(ApiClient apiClient) {
        return apiClient.getApi();
    }

    @Provides @Singleton
    ApiClient provideApiClient(RetrofitClient apiClient) {
        return apiClient;
    }

    @Provides @ApiUrl
    String provideApiUrl() {
        return getString(R.string.config_api_url);
    }
}
