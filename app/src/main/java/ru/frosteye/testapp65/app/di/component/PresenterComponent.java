package ru.frosteye.testapp65.app.di.component;

import ru.frosteye.testapp65.app.di.module.PresenterModule;
import ru.frosteye.testapp65.app.di.scope.PresenterScope;
import ru.frosteye.testapp65.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.testapp65.presentation.view.impl.activity.MainActivity;
import ru.frosteye.testapp65.presentation.view.impl.fragment.BaseFragment;

import dagger.Subcomponent;

@PresenterScope
@Subcomponent(modules = PresenterModule.class)
public interface PresenterComponent {
    void inject(BaseFragment baseFragment);

    void inject(BaseActivity baseActivity);
    void inject(MainActivity activity);
}
