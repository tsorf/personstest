package ru.frosteye.testapp65.app.environment;

/**
 * Created by oleg on 10.10.17.
 */

public class Keys {
    public static final String SPECIALITY_ID = "specialty_id";
    public static final String NAME = "name";
    public static final String F_NAME = "f_name";
    public static final String L_NAME = "l_name";
    public static final String BIRTHDAY = "birthday";
    public static final String AVATAR_URL = "avatr_url";
    public static final String SPECIALITY = "specialty";
}
