package ru.frosteye.testapp65.app.di.module;

import android.view.View;

import ru.frosteye.testapp65.app.di.scope.PresenterScope;
import ru.frosteye.testapp65.presentation.presenter.contract.MainPresenter;
import ru.frosteye.testapp65.presentation.presenter.impl.MainPresenterImpl;
import ru.frosteye.testapp65.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.testapp65.presentation.view.impl.fragment.BaseFragment;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BasePresenterModule;

@Module
public class PresenterModule extends BasePresenterModule<BaseActivity, BaseFragment> {
    public PresenterModule(View view) {
        super(view);
    }

    public PresenterModule(BaseActivity activity) {
        super(activity);
    }

    public PresenterModule(BaseFragment fragment) {
        super(fragment);
    }
    
    @Provides @PresenterScope
    MainPresenter provideMainPresenter(MainPresenterImpl presenter) {
        return presenter;
    }
}
