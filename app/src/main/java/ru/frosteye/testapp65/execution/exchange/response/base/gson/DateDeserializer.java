package ru.frosteye.testapp65.execution.exchange.response.base.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by oleg on 10.10.17.
 */

public class DateDeserializer implements JsonDeserializer<Date> {

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private DateFormat americanDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
        String date = json.getAsString();
        if(date == null || date.isEmpty()) {
            return null;
        } else {
            try {
                Date result = dateFormat.parse(date);
                if(!date.equals(dateFormat.format(result))) {
                    result = americanDateFormat.parse(date);
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
