package ru.frosteye.testapp65.execution.exchange.response.base.gson;

import ru.frosteye.ovsa.execution.serialization.AdapterItemDeserializer;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.data.model.container.PersonInfo;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonInfoDeserializer extends AdapterItemDeserializer<Person, PersonInfo> {
    @Override
    protected Class<Person> provideType() {
        return Person.class;
    }

    @Override
    protected Class<PersonInfo> provideWrapperType() {
        return PersonInfo.class;
    }
}
