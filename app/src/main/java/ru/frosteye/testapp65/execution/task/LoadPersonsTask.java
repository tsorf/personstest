package ru.frosteye.testapp65.execution.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.ApiException;
import ru.frosteye.testapp65.data.model.PersonsResult;
import ru.frosteye.testapp65.data.model.Speciality;
import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.data.model.container.SpecialityInfo;
import ru.frosteye.testapp65.data.repo.contract.PersonsRepo;
import ru.frosteye.testapp65.execution.exchange.common.Api;
import ru.frosteye.testapp65.execution.exchange.response.PersonsResponse;

/**
 * Created by oleg on 10.10.17.
 */

public class LoadPersonsTask extends NetworkTask<Boolean, PersonsResult> {

    private PersonsRepo personsRepo;

    @Inject
    public LoadPersonsTask(MainThread mainThread,
                           Executor executor,
                           Api api, PersonsRepo personsRepo) {
        super(mainThread, executor, api);
        this.personsRepo = personsRepo;
    }

    @Override
    protected Observable<PersonsResult> prepareObservable(Boolean force) {
        return Observable.create(subscriber -> {
            try {
                PersonsResponse cachedResponse = personsRepo.load();
                if(cachedResponse != null) {
                    subscriber.onNext(prepareResult(cachedResponse));
                }

                PersonsResponse response = executeCall(getApi().loadPersons());
                personsRepo.save(response);

                subscriber.onNext(prepareResult(response));
                subscriber.onComplete();
            } catch (ApiException e) {
                subscriber.onError(e);
            }
        });
    }

    private PersonsResult prepareResult(PersonsResponse response) {
        List<PersonInfo> persons = response.getData();
        Set<Speciality> set = new TreeSet<>();
        for(PersonInfo personInfo: persons) {
            set.addAll(personInfo.getModel().getSpecialities());
        }
        List<SpecialityInfo> specialities = new ArrayList<>();
        for(Speciality speciality: set) {
            specialities.add(new SpecialityInfo(speciality));
        }
        Collections.sort(persons);
        return new PersonsResult(persons, specialities);
    }
}
