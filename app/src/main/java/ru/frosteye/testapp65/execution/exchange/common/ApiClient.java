package ru.frosteye.testapp65.execution.exchange.common;

/**
 * Created by oleg on 10.10.17.
 */

public interface ApiClient {
    Api getApi();
}
