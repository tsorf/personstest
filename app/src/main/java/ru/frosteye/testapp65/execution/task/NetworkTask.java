package ru.frosteye.testapp65.execution.task;

import java.io.IOException;
import java.util.concurrent.Executor;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Response;
import ru.frosteye.ovsa.execution.executor.MainThread;
import ru.frosteye.ovsa.execution.network.base.ApiException;
import ru.frosteye.ovsa.execution.task.ObservableTask;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.execution.exchange.common.Api;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.getString;

/**
 * Created by oleg on 10.10.17.
 */

public abstract class NetworkTask<P, Result> extends ObservableTask<P, Result> {

    private Api api;

    public NetworkTask(MainThread mainThread,
                       Executor executor, Api api) {
        super(mainThread, executor);
        this.api = api;
    }

    public Api getApi() {
        return api;
    }

    protected <Res> Res executeCall(Call<Res> call) {
        try {
            Response<Res> response = call.execute();
            if(!response.isSuccessful()) {
                throw new ApiException(getString(R.string.connection_error), response.code());
            } else {
                return response.body();
            }
        } catch (IOException e) {
            throw new ApiException(getString(R.string.connection_error), 0);
        }
    }
}
