package ru.frosteye.testapp65.execution.exchange.common;

import retrofit2.Call;
import retrofit2.http.GET;
import ru.frosteye.testapp65.execution.exchange.response.PersonsResponse;

/**
 * Created by oleg on 10.10.17.
 */

public interface Api {

    @GET("testTask.json")
    Call<PersonsResponse> loadPersons();
}
