package ru.frosteye.testapp65.execution.exchange.response;

import java.util.List;

import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.execution.exchange.response.base.BaseResponse;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonsResponse extends BaseResponse<List<PersonInfo>> {
    public PersonsResponse(List<PersonInfo> response) {
        super(response);
    }
}
