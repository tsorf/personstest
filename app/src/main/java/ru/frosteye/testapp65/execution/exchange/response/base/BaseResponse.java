package ru.frosteye.testapp65.execution.exchange.response.base;

/**
 * Created by oleg on 10.10.17.
 */

public class BaseResponse<T> {

    private T response;

    public BaseResponse(T response) {
        this.response = response;
    }

    public T getData() {
        return response;
    }
}
