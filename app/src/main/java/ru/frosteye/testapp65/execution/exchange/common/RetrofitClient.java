package ru.frosteye.testapp65.execution.exchange.common;

import com.google.gson.GsonBuilder;

import java.util.Date;

import javax.inject.Inject;

import ru.frosteye.ovsa.di.qualifer.ApiUrl;
import ru.frosteye.ovsa.execution.network.client.BaseRetrofitClient;
import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.execution.exchange.response.base.gson.DateDeserializer;
import ru.frosteye.testapp65.execution.exchange.response.base.gson.PersonInfoDeserializer;

/**
 * Created by oleg on 10.10.17.
 */

public class RetrofitClient extends BaseRetrofitClient<Api> implements ApiClient {

    @Inject
    public RetrofitClient(@ApiUrl String baseUrl) {
        super(baseUrl);
    }

    @Override
    public Class<Api> apiClass() {
        return Api.class;
    }

    @Override
    protected GsonBuilder createGsonBuilder() {
        GsonBuilder builder = super.createGsonBuilder();
        builder.registerTypeAdapter(PersonInfo.class, new PersonInfoDeserializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .setDateFormat("dd-MM-yy");
        return builder;
    }
}
