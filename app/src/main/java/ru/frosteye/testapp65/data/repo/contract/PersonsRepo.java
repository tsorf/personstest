package ru.frosteye.testapp65.data.repo.contract;

import ru.frosteye.ovsa.data.storage.Repo;
import ru.frosteye.testapp65.execution.exchange.response.PersonsResponse;

/**
 * Created by oleg on 10.10.17.
 */

public interface PersonsRepo extends Repo<PersonsResponse> {
}
