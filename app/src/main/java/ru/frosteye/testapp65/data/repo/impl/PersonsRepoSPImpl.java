package ru.frosteye.testapp65.data.repo.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.BaseRepo;
import ru.frosteye.ovsa.data.storage.Storage;
import ru.frosteye.testapp65.data.repo.contract.PersonsRepo;
import ru.frosteye.testapp65.execution.exchange.response.PersonsResponse;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonsRepoSPImpl extends BaseRepo<PersonsResponse> implements PersonsRepo {


    @Inject
    public PersonsRepoSPImpl(Storage storage) {
        super(storage);
    }

    @Override
    protected Class<PersonsResponse> provideClass() {
        return PersonsResponse.class;
    }
}
