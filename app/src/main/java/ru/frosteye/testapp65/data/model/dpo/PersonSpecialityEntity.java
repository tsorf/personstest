package ru.frosteye.testapp65.data.model.dpo;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by oleg on 11.10.17.
 */

@Entity
public class PersonSpecialityEntity {
    @Id
    private Long id;
    private Long person;
    private Long speciality;
    @Generated(hash = 2029073238)
    public PersonSpecialityEntity(Long id, Long person, Long speciality) {
        this.id = id;
        this.person = person;
        this.speciality = speciality;
    }
    @Generated(hash = 2105368920)
    public PersonSpecialityEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getPerson() {
        return this.person;
    }
    public void setPerson(Long person) {
        this.person = person;
    }
    public Long getSpeciality() {
        return this.speciality;
    }
    public void setSpeciality(Long speciality) {
        this.speciality = speciality;
    }
}
