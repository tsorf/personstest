package ru.frosteye.testapp65.data.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;

import ru.frosteye.testapp65.app.environment.Keys;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by oleg on 10.10.17.
 */

public class Speciality implements Comparable<Speciality> {

    @SerializedName(Keys.SPECIALITY_ID)
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Speciality setId(int id) {
        this.id = id;
        return this;
    }

    public Speciality setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Speciality that = (Speciality) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(@NonNull Speciality o) {
        try {
            return name.compareTo(o.getName());
        } catch (Exception e) {
            return 0;
        }
    }
}
