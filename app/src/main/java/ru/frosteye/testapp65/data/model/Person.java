package ru.frosteye.testapp65.data.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.testapp65.app.environment.Keys;
import ru.frosteye.testapp65.data.model.dpo.PersonEntity;
import ru.frosteye.testapp65.data.model.dpo.SpecialityEntity;

/**
 * Created by oleg on 10.10.17.
 */

public class Person implements Comparable<Person> {

    private int id;

    @SerializedName(Keys.F_NAME)
    private String firstName;

    @SerializedName(Keys.L_NAME)
    private String lastName;

    private Date birthday;

    @SerializedName(Keys.AVATAR_URL)
    private String avatarUrl;

    @SerializedName(Keys.SPECIALITY)
    private List<Speciality> specialities;

    private int calculatedAge;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean hasSpeciality(Speciality speciality) {
        return specialities.contains(speciality);
    }

    public String getFormattedFirstName() {
        if(firstName != null && !firstName.isEmpty()) {
            return String.format("%s%s",
                    firstName.substring(0, 1).toUpperCase(),
                    firstName.substring(1, firstName.length()).toLowerCase());
        } else return null;
    }

    public String getFormattedLastName() {
        if(lastName != null && !lastName.isEmpty()) {
            return String.format("%s%s",
                    lastName.substring(0, 1).toUpperCase(),
                    lastName.substring(1, lastName.length()).toLowerCase());
        } else return null;
    }

    public String getFormattedName() {
        return String.format("%s %s", getFormattedFirstName(), getFormattedLastName());
    }

    public String getFormattedAge() {
        if(birthday == null) {
            return "-";
        } else {
            return String.valueOf(getCalculatedAge());
        }
    }

    public Person setId(int id) {
        this.id = id;
        return this;
    }

    public Person setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Person setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Person setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public Person setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public Person setSpecialities(
            List<Speciality> specialities) {
        this.specialities = specialities;
        return this;
    }

    public int getCalculatedAge() {
        if(birthday == null) return 0;
        if(calculatedAge == 0) {
            calculatedAge = DateTools.calculateAge(birthday);
        }
        return calculatedAge;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public List<Speciality> getSpecialities() {
        return specialities;
    }

    @Override
    public int compareTo(@NonNull Person o) {
        try {
            return firstName.compareTo(o.getFormattedFirstName());
        } catch (Exception e) {
            return 0;
        }
    }
}
