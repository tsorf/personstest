package ru.frosteye.testapp65.data.model.dpo;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import ru.frosteye.testapp65.data.model.Speciality;

/**
 * Created by oleg on 10.10.17.
 */

@Entity (
)
public class SpecialityEntity implements Mappable<Speciality> {
    @Id
    private Long id;
    private String name;

    @Generated(hash = 1040837621)
    public SpecialityEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 1321656953)
    public SpecialityEntity() {
    }

    @Override
    public Speciality createModel() {
        Speciality speciality = new Speciality();
        speciality.setId(getId().intValue());
        speciality.setName(getName());
        return speciality;
    }

    @Override
    public void fromModel(Speciality model) {
        setName(model.getName());
        setId((long)model.getId());
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
