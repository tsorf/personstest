package ru.frosteye.testapp65.data.model;

import java.util.ArrayList;
import java.util.List;

import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.data.model.container.SpecialityInfo;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonsResult {
    private List<PersonInfo> persons;
    private List<SpecialityInfo> specialities;

    public PersonsResult(List<PersonInfo> persons,
                         List<SpecialityInfo> specialities) {
        this.persons = persons;
        this.specialities = specialities;
    }

    public List<PersonInfo> getPersons() {
        return persons;
    }

    public List<PersonInfo> getPersonsForSpeciality(Speciality speciality) {
        List<PersonInfo> filtered = new ArrayList<>();
        for(PersonInfo personInfo: persons) {
            if(personInfo.getModel().hasSpeciality(speciality))
                filtered.add(personInfo);
        }
        return filtered;
    }

    public List<SpecialityInfo> getSpecialities() {
        return specialities;
    }
}
