package ru.frosteye.testapp65.data.model.container;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.data.model.Speciality;
import ru.frosteye.testapp65.presentation.view.impl.widget.SpecialityView;

/**
 * Created by oleg on 10.10.17.
 */

public class SpecialityInfo extends AdapterItem<Speciality, SpecialityView> {

    public SpecialityInfo(Speciality model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_speciality;
    }
}
