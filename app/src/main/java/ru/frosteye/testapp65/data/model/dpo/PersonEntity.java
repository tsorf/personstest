package ru.frosteye.testapp65.data.model.dpo;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinEntity;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.data.model.Speciality;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

/**
 * Created by oleg on 10.10.17.
 */

@Entity
public class PersonEntity implements Mappable<Person> {

    @Id(autoincrement = true)
    private Long id;

    private String firstName;
    private String lastName;
    private Date birthday;
    private String avatarUrl;

    @ToMany
    @JoinEntity(entity = PersonSpecialityEntity.class,
            sourceProperty = "person",
            targetProperty = "speciality")
    private List<SpecialityEntity> specialities;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 364927111)
    private transient PersonEntityDao myDao;



    @Generated(hash = 842956321)
    public PersonEntity(Long id, String firstName, String lastName, Date birthday,
            String avatarUrl) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.avatarUrl = avatarUrl;
    }

    public PersonEntity setSpecialities(
            List<SpecialityEntity> specialities) {
        this.specialities = specialities;
        return this;
    }

    @Generated(hash = 69356185)
    public PersonEntity() {
    }



    public List<Speciality> getPreparedSpecialities() {
        List<Speciality> list = new ArrayList<>();
        for(SpecialityEntity entity: getSpecialities()) {
            list.add(entity.createModel());
        }
        return list;
    }



    public Long getId() {
        return this.id;
    }



    public void setId(Long id) {
        this.id = id;
    }



    public String getFirstName() {
        return this.firstName;
    }



    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }



    public String getLastName() {
        return this.lastName;
    }



    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



    public Date getBirthday() {
        return this.birthday;
    }



    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }



    public String getAvatarUrl() {
        return this.avatarUrl;
    }



    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }



    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1352615448)
    public List<SpecialityEntity> getSpecialities() {
        if (specialities == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SpecialityEntityDao targetDao = daoSession.getSpecialityEntityDao();
            List<SpecialityEntity> specialitiesNew = targetDao
                    ._queryPersonEntity_Specialities(id);
            synchronized (this) {
                if (specialities == null) {
                    specialities = specialitiesNew;
                }
            }
        }
        return specialities;
    }



    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 920704523)
    public synchronized void resetSpecialities() {
        specialities = null;
    }



    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }



    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }



    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }



    @Override
    public Person createModel() {
        Person person = new Person();
        person.setFirstName(getFirstName());
        person.setLastName(getLastName());
        person.setAvatarUrl(getAvatarUrl());
        person.setBirthday(getBirthday());
        person.setSpecialities(getPreparedSpecialities());
        return person;
    }

    @Override
    public void fromModel(Person model) {
        PersonEntity entity = this;
        entity.setAvatarUrl(model.getAvatarUrl());
        entity.setBirthday(model.getBirthday());
        entity.setFirstName(model.getFirstName());
        entity.setLastName(model.getLastName());
        List<SpecialityEntity> specs = new ArrayList<>();
        for(Speciality spec: model.getSpecialities()) {
            SpecialityEntity specialityEntity = new SpecialityEntity();
            specialityEntity.setId((long)spec.getId());
            specialityEntity.setName(spec.getName());
            specs.add(specialityEntity);
        }
        entity.setSpecialities(specs);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 928559693)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPersonEntityDao() : null;
    }
}
