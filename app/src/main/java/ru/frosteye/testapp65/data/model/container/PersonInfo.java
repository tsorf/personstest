package ru.frosteye.testapp65.data.model.container;

import android.support.annotation.NonNull;

import ru.frosteye.ovsa.presentation.adapter.AdapterItem;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.presentation.view.impl.widget.PersonView;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonInfo extends AdapterItem<Person, PersonView> implements Comparable<PersonInfo> {

    public PersonInfo() {
    }

    public PersonInfo(Person model) {
        super(model);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.view_person;
    }

    @Override
    public int compareTo(@NonNull PersonInfo o) {
        return getModel().compareTo(o.getModel());
    }
}
