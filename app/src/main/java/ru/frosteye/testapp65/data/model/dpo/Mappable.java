package ru.frosteye.testapp65.data.model.dpo;

/**
 * Created by oleg on 10.10.17.
 */

public interface Mappable<T> {
    T createModel();
    void fromModel(T model);
}
