package ru.frosteye.testapp65.data.repo.impl;

import android.content.Context;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.data.model.Speciality;
import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.data.model.dpo.DaoMaster;
import ru.frosteye.testapp65.data.model.dpo.DaoSession;
import ru.frosteye.testapp65.data.model.dpo.PersonEntity;
import ru.frosteye.testapp65.data.model.dpo.PersonSpecialityEntity;
import ru.frosteye.testapp65.data.model.dpo.SpecialityEntity;
import ru.frosteye.testapp65.data.repo.contract.PersonsRepo;
import ru.frosteye.testapp65.execution.exchange.response.PersonsResponse;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonsRepoSqliteImpl implements PersonsRepo {

    public static final String DB_NAME = "persons-db";

    private Context context;
    private DaoMaster master;
    private DaoSession session;

    @Inject
    public PersonsRepoSqliteImpl(Context context) {
        this.context = context;
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, DB_NAME);
        Database db = helper.getWritableDb();
        master = new DaoMaster(db);
        session = master.newSession();
    }

    @Override
    public synchronized PersonsResponse load() {
        List<PersonEntity> all = session.getPersonEntityDao().loadAll();
        List<PersonInfo> infos = new ArrayList<>();
        for(PersonEntity person: all) {
            infos.add(new PersonInfo(person.createModel()));
        }
        if(infos.isEmpty()) return null;
        return new PersonsResponse(infos);
    }

    @Override
    public synchronized void save(PersonsResponse personsResponse) {
        session.getPersonSpecialityEntityDao().deleteAll();
        session.getPersonEntityDao().deleteAll();
        session.getSpecialityEntityDao().deleteAll();
        for(PersonInfo info: personsResponse.getData()) {
            PersonEntity entity = new PersonEntity();
            entity.fromModel(info.getModel());
            session.getSpecialityEntityDao().insertOrReplaceInTx(entity.getSpecialities(), true);
            session.getPersonEntityDao().insert(entity);
            List<PersonSpecialityEntity> joins = new ArrayList<>();
            for(SpecialityEntity specialityEntity: entity.getSpecialities()) {
                PersonSpecialityEntity join = new PersonSpecialityEntity();
                join.setPerson(entity.getId());
                join.setSpeciality(specialityEntity.getId());
                joins.add(join);
            }
            session.getPersonSpecialityEntityDao().insertOrReplaceInTx(joins);
        }
    }

    @Override
    public synchronized void executeAndSave(RepoRunnable<PersonsResponse> runnable) {
        throw new RuntimeException("not implemented");
    }
}
