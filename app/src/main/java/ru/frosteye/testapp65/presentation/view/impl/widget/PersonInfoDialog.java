package ru.frosteye.testapp65.presentation.view.impl.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.data.model.Speciality;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonInfoDialog extends Dialog implements ModelView<Person> {

    @BindView(R.id.view_personExtended_birthday) TextView birthday;
    @BindView(R.id.view_personExtended_person) PersonView personView;
    @BindView(R.id.view_personExtended_specialities) TextView specialities;

    private Person model;

    public PersonInfoDialog(@NonNull Context context) {
        super(context);
    }

    public PersonInfoDialog(@NonNull Context context,
                            @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected PersonInfoDialog(@NonNull Context context, boolean cancelable,
                               @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public Person getModel() {
        return model;
    }

    @Override
    public void setModel(Person model) {
        this.model = model;
        if(birthday == null) {
            throw new RuntimeException("PersonInfoDialog is not initialized");
        }
        String bDate = "-";
        if(model.getBirthday() != null) {
            bDate = DateTools.formatDottedDate(model.getBirthday());
        }
        this.birthday.setText(bDate);
        this.personView.setModel(model);
        StringBuilder builder = new StringBuilder();
        for(Speciality speciality: model.getSpecialities()) {
            builder.append(", ").append(speciality.getName());
        }
        if(builder.length() > 0) {
            specialities.setText(builder.toString().substring(2));
        } else {
            specialities.setText(R.string.no_speciality);
        }
    }

    public void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View content = LayoutInflater.from(getContext()).inflate(R.layout.view_person_extended, null);
        setContentView(content);
        ButterKnife.bind(this, content);
    }

    public static void createAndShow(Activity context, Person person) {
        PersonInfoDialog dialog = new PersonInfoDialog(context);
        dialog.init();
        dialog.setModel(person);
        dialog.show();
    }
}
