package ru.frosteye.testapp65.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;
import ru.frosteye.testapp65.data.model.PersonsResult;

public interface MainView extends BasicView {
    void showPersonsResult(PersonsResult result);
}
