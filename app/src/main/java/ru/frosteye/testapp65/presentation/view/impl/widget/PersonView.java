package ru.frosteye.testapp65.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.data.model.Person;

/**
 * Created by oleg on 10.10.17.
 */

public class PersonView extends BaseLinearLayout implements ModelView<Person> {

    @BindView(R.id.view_person_age) TextView age;
    @BindView(R.id.view_person_name) TextView name;
    @BindView(R.id.view_person_avatar) ImageView image;

    private Person model;

    public PersonView(Context context) {
        super(context);
    }

    public PersonView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PersonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    @Override
    public Person getModel() {
        return model;
    }

    @Override
    public void setModel(Person model) {
        this.model = model;
        if(model.getAvatarUrl() != null && !model.getAvatarUrl().isEmpty()) {
            Picasso.with(getContext()).load(model.getAvatarUrl()).fit().centerCrop().into(image);
        } else {
            image.setImageDrawable(null);
        }
        age.setText(model.getFormattedAge());
        name.setText(model.getFormattedName());
    }
}
