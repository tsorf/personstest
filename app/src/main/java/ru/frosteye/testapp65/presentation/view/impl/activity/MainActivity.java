package ru.frosteye.testapp65.presentation.view.impl.activity;

import javax.inject.Inject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFlexible;
import ru.frosteye.ovsa.presentation.adapter.FlexibleModelAdapter;
import ru.frosteye.ovsa.presentation.view.widget.ListDivider;
import ru.frosteye.testapp65.app.di.component.PresenterComponent;
import ru.frosteye.testapp65.data.model.Person;
import ru.frosteye.testapp65.data.model.PersonsResult;
import ru.frosteye.testapp65.data.model.Speciality;
import ru.frosteye.testapp65.data.model.container.PersonInfo;
import ru.frosteye.testapp65.data.model.container.SpecialityInfo;
import ru.frosteye.testapp65.presentation.presenter.contract.MainPresenter;
import ru.frosteye.testapp65.presentation.view.contract.MainView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.presentation.view.impl.widget.PersonInfoDialog;

public class MainActivity extends BaseActivity implements MainView, FlexibleAdapter.OnItemClickListener {

    @BindView(R.id.activity_main_list) RecyclerView list;
    @BindView(R.id.activity_main_swipe) SwipeRefreshLayout swipe;

    @Inject MainPresenter presenter;

    private FlexibleModelAdapter<IFlexible> adapter;

    private PersonsResult personsResult;
    private boolean personsMode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        showTopBarLoading(!enabled);
        if(enabled) swipe.setRefreshing(false);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        adapter = new FlexibleModelAdapter<>(new ArrayList<>(), this);
        list.addItemDecoration(new ListDivider(this, ListDivider.VERTICAL_LIST));
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.onLoadPersons(true));
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
        presenter.onLoadPersons(true);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void showPersonsResult(PersonsResult result) {
        this.personsResult = result;
        if(!personsMode) {
            fillSpecialities();
        }
    }

    private void fillSpecialities() {
        personsMode = false;
        setTitle(R.string.specialities);
        adapter.updateDataSet(new ArrayList<>(personsResult.getSpecialities()), true);
    }

    private void fillPersons(Speciality speciality) {
        personsMode = true;
        List<PersonInfo> filtered = personsResult.getPersonsForSpeciality(speciality);
        setTitle(getString(R.string.pattern_persons_counter, speciality.getName(), filtered.size()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter.updateDataSet(new ArrayList<>(filtered), true);
    }

    private boolean handleBack() {
        if(personsMode) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            fillSpecialities();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        if(!handleBack()) super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            handleBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onItemClick(int position) {
        if(personsMode) {
            Person person = ((PersonInfo) adapter.getItem(position)).getModel();
            PersonInfoDialog.createAndShow(this, person);
        } else {
            Speciality speciality = ((SpecialityInfo) adapter.getItem(position)).getModel();
            fillPersons(speciality);
        }
        return true;
    }
}
