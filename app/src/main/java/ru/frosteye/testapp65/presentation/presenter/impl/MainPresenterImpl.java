package ru.frosteye.testapp65.presentation.presenter.impl;

import javax.inject.Inject;

import ru.frosteye.ovsa.execution.task.SimpleSubscriber;
import ru.frosteye.testapp65.data.model.PersonsResult;
import ru.frosteye.testapp65.execution.task.LoadPersonsTask;
import ru.frosteye.testapp65.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.testapp65.presentation.presenter.contract.MainPresenter;

public class MainPresenterImpl extends BasePresenter<MainView> implements MainPresenter {

    private LoadPersonsTask loadPersonsTask;

    @Inject
    public MainPresenterImpl(LoadPersonsTask loadPersonsTask) {

        this.loadPersonsTask = loadPersonsTask;
    }

    @Override
    public void onDestroy() {
        loadPersonsTask.cancel();
    }

    @Override
    public void onLoadPersons(boolean force) {
        enableControls(false);
        loadPersonsTask.execute(force, new SimpleSubscriber<PersonsResult>() {
            @Override
            public void onComplete() {
                enableControls(true);
            }

            @Override
            public void onError(Throwable e) {
                enableControls(true);
                showMessage(e.getMessage());
            }

            @Override
            public void onNext(PersonsResult result) {
                view.showPersonsResult(result);
            }
        });
    }
}
