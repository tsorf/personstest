package ru.frosteye.testapp65.presentation.view.impl.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.ModelView;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.testapp65.R;
import ru.frosteye.testapp65.data.model.Speciality;

/**
 * Created by oleg on 10.10.17.
 */

public class SpecialityView extends BaseLinearLayout implements ModelView<Speciality> {

    @BindView(R.id.view_speciality_name) TextView name;

    private Speciality model;

    public SpecialityView(Context context) {
        super(context);
    }

    public SpecialityView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpecialityView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SpecialityView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
    }

    @Override
    public Speciality getModel() {
        return model;
    }

    @Override
    public void setModel(Speciality model) {
        this.model = model;
        this.name.setText(model.getName());
    }
}
