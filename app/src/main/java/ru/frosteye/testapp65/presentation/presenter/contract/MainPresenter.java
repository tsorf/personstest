package ru.frosteye.testapp65.presentation.presenter.contract;

import ru.frosteye.testapp65.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface MainPresenter extends LivePresenter<MainView> {
    void onLoadPersons(boolean force);
}
